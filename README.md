ImportNoSQL
===========

This script allows you to retrieve data from import.io and store it directly in NoSQL databases.

At this moment you can save data into Firebase, MongoDB and Elasticsearch. Everything you need to do is fiil up the config file (config.json).

How it works
===========

Just run python importNoSQL.py [--database]

where --database can be --firebase, --mongo and --elastic

Mandatory fields for Firebase:

* All of them;

Mandatory fields for MongoDB:

* HOST, PORT

Mandatory fields for Elasticsearch:

* HOST, PORT, DB, COLLECTION

config.json
===========

This file is everything you need to run the script. Once filled up, all data will be stored into the database technology you choose.

TO-DO
===========

* Default values for DB and COLLECTION for Elasticsearch;

* MongoDB authentication;

* Check if a piece of data already exists; If so we update the old data instead of inserting/replicate it (does it make sense?);

* Abstract a bit more to be easier to add new db technologies (it is already easy but it can be more).