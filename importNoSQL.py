import argparse
import hashlib
import json
import requests
import sys
import time
import urllib

from elasticsearch import Elasticsearch
from elasticsearch.exceptions import ConnectionError
from firebase import firebase
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure
from requests.exceptions import \
    Timeout, \
    ConnectionError

#own libraries
from import_errors import NoDataFound

#TODO

# Change to one config dictionary instead of one per technology
# Update old results instead of adding again?
# Authentication for MongoDB?

IO_URL = "https://api.import.io/store/data/{source}/_query?_user={user}&_apikey={api}"
CHARS_TO_REMOVE = "./_@?!" # Chars to be removed from the key to be stored in Firebase

TECHNOLOGIES = ['FIREBASE', 'MONGODB', 'ELASTICSEARCH']
REQUIRED_FB_CONFS = [
    'TOKEN', 
    'EMAIL', 
    'URL', 
    'RESOURCE', 
    'P_KEY'
]

REQUIRED_MDB_CONFS = ['HOST', 'PORT']
REQUIRED_IO_CONFS = ['IO_USER', 'IO_API', 'IO_SOURCE']
REQUIRED_ES_CONFS = ['HOST', 'PORT', 'DB', 'COLLECTION']

DEFAULTS = {"DB" : "import_data", "COLLECTION" : "data"} # MongoDB default values if not provided in config.json

class Import(object):
    
    def __init__(self, conf_fn, db_tech):
        self.db_tech = db_tech
        self.io_confs = {}
        self.get_confs(conf_fn)
        
        self.create_connection()
    
    def create_connection(self):
        """
            Create a connection with the DB.
            
            ** To be extended and implemented in child classes **
        """
        pass
    
    def get_confs(self, fn):
        """
            Get all configuration data for import.io and all db technologies
            
            Args:
                fn: the json filename to be parsed
            
            Returns:
                confs: the dictionary containing all configurations
        """
        with open(fn) as file:
            confs = json.load(file)
        file.close()
        
        if self.db_tech not in TECHNOLOGIES:
            print "You didn't specify any DB technology."
            sys.exit(1)
        
        # Getting and checking Firebase configs
        if self.db_tech == 'FIREBASE' and \
            not all(c in confs[self.db_tech] for c in REQUIRED_FB_CONFS):
            print "Some required config for Firebase is missing. Required attributes: {req}".format(req=REQUIRED_FB_CONFS)
            sys.exit(1)
        else:
            self.fb_confs = confs['FIREBASE']
        
        # Getting and checking MongoDB configs
        if self.db_tech == 'MONGODB' and \
            not all(c in confs[self.db_tech] for c in REQUIRED_MDB_CONFS):
            print "Some required config for MongoDB is missing. Required attributes: {req}".format(req=REQUIRED_MDB_CONFS)
            sys.exit(1)
        else:
            self.mongo_confs = confs['MONGODB']
            
         # Getting and checking Elasticsearch configs
        if self.db_tech == 'ELASTICSEARCH' and \
            not all(c in confs[self.db_tech] for c in REQUIRED_ES_CONFS):
            print "Some required config for Firebase is missing. Required attributes: {req}".format(req=REQUIRED_ES_CONFS)
            sys.exit(1)
        else:
            self.es_confs = confs['ELASTICSEARCH']
        
        # Getting and checking import.io configs
        if not all(c in confs['IMPORTIO'] for c in REQUIRED_IO_CONFS):
            print "Some required config for Import.io is missing. Required attributes: {req}".format(req=REQUIRED_IO_CONFS)
            sys.exit(1)
        else:
            self.io_confs = confs['IMPORTIO']
        
        return confs
        
    def get_data(self, url):
        """
            Do a request to import.io for a given url
            
            Args:
                url: the url to be requested
            
            Returns:
                results: the results returned from import.io
        """
        
        def _do_request(io_url, data, tentative=1, timeout=5, waiting_secs=5):
            """
                Do requests to a given url
        
                Args:
                    io_url: the url to be requested
                    data: the body data to be sent with the request
                    tentative: the number of times to try if a connection gives timeout
                    
                Raises:
                    NoDataFound: when a reply is empty
                    
                Returns:
                    text: the body response
            """
            
            try:
                r = requests.post(io_url, data=data, timeout=timeout)
                
                if r.status_code >= 200 and r.status_code <= 300:
                    return json.loads(r.text)
                else:
                    print "No data returned from request."
                    raise NoDataFound
            
            except Timeout, e:
                if tentative > 5:
                    print "Got 5 consecutive timeouts. Exiting."
                    sys.exit(1)
                
                print "Connection timeout. Trying again in {secs}.".format(secs=waiting_secs)
                time.sleep(waiting_secs)
                _do_request(io_url, data, tentative + 1, timeout + 5)
            
            except ConnectionError, e:
                print "Error connecting to import.io: {error}".format(error=e)
                sys.exit(1)
        
        def _format_req_data(url):
            """
                Format the message containing an url to be passed to import.io
                
                Args:
                    url: the url to be formatted into the message
            """
            
            body = json.dumps({
                        "input": {
                            "webpage/url": url
                        }
                    }, indent=4)
            
            return body
        
        response = _do_request(IO_URL.format(
                            source=self.io_confs['IO_SOURCE'], 
                            user=self.io_confs['IO_USER'], 
                            api=urllib.quote(self.io_confs['IO_API'])
                        ), _format_req_data(url))
                        
        if response:
            return response['results']
            
        
    def get_io_data(self):
        """
            Get data from import.io for all urls provided and store it into Firebase
                
            ** To be extended and implemented in child classes **
        """
        pass
    
    def handle_custom_data(self, results):
        """
            Get only the pre-defined keys provided in the config file. All other data will be ignored and not stored
            
            Args:
                results: the results from import.io to be parsed
                
            Returns:
                data: the list containing all results that fit the attributes provided
        """
        
        data = []
        
        # If we have filtered attributes we only want to parse those
        if len(self.io_confs['IO_ATTRIBS']) > 0:
            
            for entry in results:
                selected_data = {}
                
                for key in self.io_confs['IO_ATTRIBS']:
                    if key in entry:
                        selected_data[key] = entry[key]
    
                data.append(selected_data)
        
            return data
        
        # If we do not have filtered attributes but mappings
        elif len(self.io_confs['IO_ATTR_MAPPING']) > 0:
                
            for entry in results:
                selected_data = {}
                
                for key, mapping in self.io_confs['IO_ATTR_MAPPING'].items():
                    if mapping in entry:
                        selected_data[key] = entry[mapping]
        
                data.append(selected_data)
            
            return data
        
        return results
        
    def insert(self, func):
        """
            Wrapper to execute specific insert function from different db technologies
            
            Args:
                func: the insertion function to be executed
            
            Returns:
                the result from the insertion action
        """
        
        def insert_db(*args, **kwargs):
            return func(*args, **kwargs)
        
        return insert_db
    
    def check_optional_config(self):
        """
            Default some options don't being provided in config file
            
            Configurations like db name or collection name are optional. If they are not provided in the config file
                we need to use the default ones
        """
        
        confs = None
        
        if self.db_tech == 'MONGODB':
            confs = self.mongo_confs
        elif self.db_tech == 'ELASTICSEARCH':
            confs = self.es_confs
            
        if not confs['DB']:
            print "No {tech} DB provided in config.json. Will store data in {db}".format(tech=self.db_tech, db=DEFAULTS['DB'])
            confs['DB'] = DEFAULTS['DB']

        if not confs['COLLECTION']:
            print "No {tech} COLLECTION provided in config.json. Will store data in {db}".format(tech=self.db_tech, db=DEFAULTS['COLLECTION'])
            confs['COLLECTION'] = DEFAULTS['COLLECTION']
        
class ImportFB(Import):
    
    def __init__(self, conf_fn, db_tech):
        self.fb_confs = {}
        self.authentication = None
        self.fb = None
        Import.__init__(self, conf_fn, db_tech)
        
    def create_connection(self):
        self.authentication = firebase.FirebaseAuthentication(self.fb_confs['TOKEN'], self.fb_confs['EMAIL'])
        self.fb = firebase.FirebaseApplication(self.fb_confs['URL'], self.authentication)
        
    def get_io_data(self):
        """
            Get data from import.io for all urls provided and store it into Firebase
            
        """
        
        if len(self.io_confs['IO_URLS']):
            for url in self.io_confs['IO_URLS']:
                results = self.get_data(url)
                self.insertFB(results)
                
        else:
            print "No URLs provided in IO_URL in config.json"
            sys.exit(1)
    
    def insertFB(self, results):
        """
            Insert the results from import.io into the Firebase
            
            If some attributes were provided in the config file, only those attributes will be parsed and stored.
            
            Args:
                data: the results from import.io
        """
        
        data = self._serialize_data(self.handle_custom_data(results))
        self.insert(self.fb.put(self.fb_confs['URL'], self.fb_confs['RESOURCE'], data))
        
    def _serialize_data(self, results):
        """
            Remove special characters from result keys.
            
            Firebase do not accept special characters in the key string so 
                we need to figure out a way to "encode" it and at the same time it should still be searchable
            
            Args:
                results: the results from import.io
                
            Returns:
                data: the results without special characters in their keys
        """
        
        data = {}
        
        for entry in results:
            key_hash = hashlib.md5(json.dumps(entry[self.fb_confs['P_KEY']])).hexdigest()
            data[key_hash] = {}
            for key, value in entry.items():
                final_key = key.encode('utf8').translate(None, CHARS_TO_REMOVE)
                data[key_hash][final_key] = value.encode('utf8')
        
        return data
            
class ImportMDB(Import):
    
    def __init__(self, conf_fn, db_tech):
        self.mongo_confs = {}
        self.client = None
        self.db = None
        self.collection = None
        Import.__init__(self, conf_fn, db_tech)
        
    def create_connection(self):
        """
            Create a connection with MongoDB Database given the configuration
        """
        
        self.check_optional_config()

        # Creating mongodb connection
        try:
            self.client = MongoClient(self.mongo_confs['HOST'], self.mongo_confs['PORT'])
            self.db = self.client[self.mongo_confs['DB']]
            self.collection = self.db[self.confs['COLLECTION']]
            
        except ConnectionFailure, e:
            print "Error connecting to MongoDB: {error}".format(error=e)
            sys.exit(1)
            
    def insertMDB(self, data):
        """
            Insert the results from import.io into the MongoDB collection.collection
            
            If some attributes were provided in the config file, only those attributes will be parsed and stored.
            
            Args:
                data: the results from import.io
        """
        
        data = self.handle_custom_data(data)
        result = self.insert(self.collection.insert(data))
        
    def get_io_data(self):
        """
            Get data from import.io for all urls provided and store it into MongoDB
            
        """
        
        if len(self.io_confs['IO_URLS']):
            for url in self.io_confs['IO_URLS']:
                results = self.get_data(url)
                self.insertMDB(results)
        else:
            print "No URLs provided in IO_URL in config.json"
            sys.exit(1)


class ImportES(Import):
    
    def __init__(self, conf_fn, db_tech):
        self.es_confs = {}
        self.es = None
        Import.__init__(self, conf_fn, db_tech)
        
    def create_connection(self):
        try:
            self.es = Elasticsearch(host=self.es_confs['HOST'], port=self.es_confs['PORT'])
        except ConnectionError, e:
            print "Error connecting to Elasticsearch: {error}".format(error=e)
            sys.exit(1)
        
        self.check_optional_config()
        
    def get_io_data(self):
        """
            Get data from import.io for all urls provided and store it into MongoDB
            
        """
        
        if len(self.io_confs['IO_URLS']):
            for url in self.io_confs['IO_URLS']:
                results = self.get_data(url)
                self.insertES(results)
        else:
            print "No URLs provided in IO_URL in config.json"
            sys.exit(1)
            
    def insertES(self, data):
        """
            Insert the results from import.io into the Elasticsearch database.
            
            If some attributes were provided in the config file, only those attributes will be parsed and stored.
            
            Args:
                data: the results from import.io
        """
        
        data = self.handle_custom_data(data)
        
        for entry in data:
            try:
                result = self.insert(self.es.index(index=self.es_confs['DB'], doc_type=self.es_confs['COLLECTION'], body=entry))
            except ConnectionError, e:
                print "Connection error while trying to insert into Elasticsearch."


################## MAIN ################## 

def store_firebase(tech='FIREBASE'):
    ImportFB('config.json', tech).get_io_data()

def store_mongodb(tech='MONGODB'):
    ImportMDB('config.json', tech).get_io_data()

def store_elasticsearch(tech='ELASTICSEARCH'):
    ImportES('config.json', tech).get_io_data()
    
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='Store import.io results into NoSQL DB.')
    
    parser.add_argument('--firebase', dest='firebase', action='store_true', help='Storing into Firebase.')
    parser.add_argument('--mongo', dest='mongodb', action='store_true', help='Storing into MongoDB.')
    parser.add_argument('--elastic', dest='elasticsearch', action='store_true', help='Storing into Elasticsearch.')
    
    args = parser.parse_args()
    if args.firebase:
        store_firebase()
    if args.mongodb:
        store_mongodb()
    if args.elasticsearch:
        store_elasticsearch()
